import React, { useState, useEffect } from 'react';
// import './Users.css'
import { useSelector, useDispatch } from 'react-redux';
import { addUsers, deleteUser, updateName } from './reducers/userReducer';
import { Link } from 'react-router-dom';
import RecordDrop from './dropDown/RecordDrop';
import axios from 'axios';



const Users = () => {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.users.value) 

  const [data, setData] = useState([])


const [name, setName] = useState('')
const [surname, setSurname] = useState('')
const [telephone, setTelephone] = useState('')
const [photo, setPhoto] = useState('')

const [newName, setNewName] = useState('')
const [newSurname, setNewSurname] = useState('')

// setName('')
// setSurname('')
// setTelephone('')
// setNewName('')
// setNewSurname('')


useEffect(() => {
  axios.get('http://localhost:3300/user')
.then((res) => {
  setData(res.data)
})
.catch(err => err)
},[])



return (
    <div>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    <RecordDrop/>
    <Link to='/'>Sign out</Link>
    </div>

      <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border  bg-secondary text-white p-5'>
    <h1>Add Users</h1>
        <div>
      <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='surname'>Surname:</label>
        <input type='text' name='surname'  className='form-control' placeholder='Enter Surname' 
        onChange={e => setSurname(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='telephone'>Telephone:</label>
        <input type='phone' name='telephone'  className='form-control' placeholder='Enter Surname' 
        onChange={e => setTelephone(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
        <br/>
        <button className='btn btn-info' onClick={() => {dispatch(addUsers({id: userData[userData.length - 1].id + 1, name, surname, telephone, photo}))}}>Add User</button>
        
        
        </div>

        </div>

        </div>




{/* Displaying users from userList */}
      <div className='displayUsers1'>
{userData.map((user) => {
  return <div className='displayUsers'>
  <div key={user}>
  <div><img src={user.photo} alt='Pics'/></div>
  <p><span style={{}}>Name:</span> <span style={{}}>{user.name}</span></p>
  <p><span style={{}}>Surname:</span> <span style={{}}>{user.surname}</span></p>
  <p><span style={{}}>Telephone:</span> <span style={{}}>{user.telephone}</span></p>

  {/* To update and delete user's information */}
  <div className='botin'>
  <input type='text' placeholder='New Name....' className='form-control' onChange={(e) => {setNewName(e.target.value)}}/>
  <input type='text'  placeholder='New Surname....' className='form-control mt-1' onChange={(e) => {setNewSurname(e.target.value)}}/>
    <button className='btn btn-success me-2 mt-1' onClick={() => {dispatch(updateName({id: user.id, name:newName, surname:newSurname}))}}><strong>Update</strong></button>
    <button className='btn btn-danger mt-1'  onClick={() => {dispatch(deleteUser({id: user.id}))}}><strong>Delete</strong></button>
</div>
</div>
</div>


})}
      </div>
      </div>

  );
}

export default Users;

