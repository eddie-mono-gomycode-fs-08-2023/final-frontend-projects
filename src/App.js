import './App.css';
import { Routes, Route } from 'react-router-dom';
import Navbar from './Navbar';
import Layout from './Layout';
import Home from './Home';
import Users from './Users';
import Update from './Update';
import LoginAdmin from './LoginAdmin';
import Register from './Register';
import RecordDrop from './dropDown/RecordDrop';
import Engine from './Engine';
import Company from './Company';
import Type from './Type';
import TypeCreate from './TypeCreate';
import Space from './Space';
import Parking from './Parking';
import PhoneNavDrop from './dropDown/PhoneNavDrop';
import Requet from './Requet';
import RequetCreate from './RequetCreate';
import Login from './Login';
import Login2 from './Login2';
import Signu2 from './Signu2';
import Wash from './Wash';
import Rentals from './Rentals';
import Mentainance from './Mentainance';
import ParkingSpace from './ParkingSpace';





function App() {
  return (

    <div className="App">
      <Navbar/>

<Routes>
<Route index element={<Layout/>} />
<Route path='/home' element={<Home/>}/>
<Route path='/users' element={<Users/>}/>
<Route path='/edit/:id' element={<Update/>}/>
<Route path='/adminLog' element={<LoginAdmin/>}/>
<Route path='/register' element={<Register/>}/>
<Route path='/record' element={<RecordDrop/>}/>
<Route path='/engine' element={<Engine/>}/>
<Route path='/company' element={<Company/>}/>
<Route path='/types' element={<Type/>}/>
<Route path='/typeCreate' element={<TypeCreate/>}/>
<Route path='/space' element={<Space/>}/>
<Route path='/parking' element={<Parking/>}/>
<Route path='/phoneNav' element={<PhoneNavDrop/>}/>
<Route path='/requets' element={<Requet/>}/>
<Route path='/requetCreate' element={<RequetCreate/>}/>
<Route path='/login' element={<Login/>}/>
<Route path='/login2' element={<Login2/>}/>
<Route path='/signup2' element={<Signu2/>}/>
<Route path='/wash' element={<Wash/>}/>
<Route path='/rentals' element={<Rentals/>}/>
<Route path='/mentain' element={<Mentainance/>}/>
<Route path='/parkingspace' element={<ParkingSpace/>}/>

</Routes>


    </div>
  );
}

export default App;
