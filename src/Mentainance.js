import React from 'react';
import './Wash.css'
import { Link } from 'react-router-dom';

const Mentainance = () => {
  const videoBg = 'topVideo.mp4'
  return (
    <div className='wash w-100 vh-100'>
    <div className='overlay'></div>
      <video src={videoBg} type='video/mp4' autoPlay muted loop/>
      <div className='content'>
        <h1>Car Wash</h1>
        <p>The best car mentainance company</p>
        <Link to='/parking' className='btn btn-success rounded shadow'>Go to parking</Link>

      </div>
      
    </div>
  );
}

export default Mentainance;
