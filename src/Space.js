import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';
import { addSpaces, deleteSpace, updateSpace } from './reducers/spaceReducer';
import axios from 'axios';



const Space = () => {
  const spaceData = useSelector((state) => state.spaces.value)
  console.log(spaceData);
const dispatch = useDispatch()

const [data, setData] = useState('')
const [description, setDescription] = useState('')
  const [localization, setLocalization] = useState('')
  const [photo, setphoto] = useState('')

  const [newDescription, setNewDescription] = useState('')
  const [newLocation, setNewLocation] = useState('')



const handleDelete = (id) => {
dispatch(deleteSpace({id: id}))
}

useEffect(() => {
  axios.get('http://localhost:3300/spaces')
.then((res) => {
  setData(res.data)
})
.catch(err => err)
},[])

  return (
    <div className='parkingSpace w-100 bg-secondary d-flex flex-column justify-content-center align-items-center'>
    <div className='Home-nav '>
    <Link to='/home'>Home</Link>
    {/* <Link to='/engine'>Record Engine</Link> */}

<RecordDrop/>
    {/* <Link to='/parking'>Parking Space</Link> */}
    {/* <Link to='/service'>Services</Link> */}
    {/* <Link to='/users'>Users</Link> */}
    <Link to='/'>Sign out</Link>
    </div>

    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border  bg-secondary text-white p-5'>
    <h1>Add Space</h1>

    <div>
        <label htmlFor='description'>Description:</label>
        <input type='text' name='description' className='form-control' placeholder='Spcae Description' 
        onChange={e => setDescription(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='localization'>Localization:</label>
        <input type='text' name='localization' className='form-control' placeholder='Branch Address' 
        onChange={e => setLocalization(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Add Photo' 
        onChange={e => setphoto(e.target.value)}/>
        <br/>
        <button className='btn btn-info' onClick={() => {dispatch(addSpaces({id: spaceData[spaceData.length - 1].id + 1, description, localization, photo}))}}>Add Space</button>
        </div>
        </div>
        </div>





    <div className='container w-100' style={{display:'flex', justifyContent:'center', alignItems:'center'}}>
    <h3>Space Record</h3>
    <table className='table w-100'>
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPTION</th>
          <th>LOCALIZATION</th>
          <th>PHOTO</th>
          <th>ACTIONS</th>

        </tr>
      </thead>
{spaceData.map((space, index) => {
  return(
    <tr key={index}>
  <td>{space.id}</td>
  <td>{space.description}</td>
  <td>{space.localization}</td>
  <td><img src={space.photo} alt='Pics'/></td>
<td>
  <input type='text' className='form-control mb-2' placeholder='New Name....' onChange={(e) => {setNewDescription(e.target.value)}}/>
  <input type='text' className='form-control mb-2' placeholder='New Location....' onChange={(e) => {setNewLocation(e.target.value)}}/>
<button className='btn btn-success bg-success me-3 rounded' onClick={() => {dispatch(updateSpace({id: space.id, description: newDescription, localization:newLocation}))}}>Update Record</button>
<button className='btn btn-danger bg-danger rounded'  onClick={() => {dispatch(deleteSpace({id: space.id}))}}>Delete Record</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>

    </div>

  );
}

export default Space;
