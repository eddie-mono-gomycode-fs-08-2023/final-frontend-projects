import React from 'react';

const Login2 = () => {
  return (
    <div>
      <form action='login2' method='post'>
    <input type='email' name='email' placeholder='Enter Email'/>
    <input type='password' name='password' placeholder='Enter password'/>
    <input type='submit'/>
    </form>
    <h2>Don't have an account?</h2><a href="/signup2">Sign Up</a>
    </div>
  );
}

export default Login2;
