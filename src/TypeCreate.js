import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom';
import { addType } from './reducers/typeReducer';

const TypeCreate = () => {
  const [description, setDescription] = useState('')

const types = useSelector((state) => state.types.types)
console.log(types)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createType = (e) => {
e.preventDefault()
dispatch(addType({id: Date.now(), description}))
Navigate('/types')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Engine Information</h1>
    <form onSubmit={createType}>
      <div>
        <label htmlFor='description'>Description:</label>
        <input type='text' name='description' className='form-control' placeholder='Describe Your Engine' 
        onChange={e => setDescription(e.target.value)}/>
      </div>
      <button className='btn btn-info'>Submit</button>
    </form>
    </div>
    </div>
  );
}

export default TypeCreate;
