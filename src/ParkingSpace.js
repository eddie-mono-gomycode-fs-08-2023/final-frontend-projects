import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector,  } from 'react-redux';
import { Link } from 'react-router-dom';
import './Users.css'
import RecordDrop from './dropDown/RecordDrop';
import { deleteSpace, } from './reducers/spaceReducer';
import ServiceDrop from './dropDown/ServiceDrop';
import axios from 'axios';



const ParkingSpace = () => {
  const spaceData = useSelector((state) => state.spaces.value)
  console.log(spaceData);
const dispatch = useDispatch()

const [data, setData] = useState('')
const [description, setDescription] = useState('')
  const [localization, setLocalization] = useState('')
  const [photo, setphoto] = useState('')

  const [newDescription, setNewDescription] = useState('')
  const [newLocation, setNewLocation] = useState('')



const handleDelete = (id) => {
dispatch(deleteSpace({id: id}))
}

useEffect(() => {
  axios.get('http://localhost:3300/spaces')
.then((res) => {
  setData(res.data)
})
.catch(err => err)
},[])

  return (
    <div className='parkingSpace w-100 bg-secondary d-flex flex-column justify-content-center align-items-center'>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    <ServiceDrop/>
  <Link to='/'>Sign out</Link>
    </div>

    <div className='container w-100 mt-5'>
    <h3>Available Parking Space</h3>
    <table className='table bg-light w-50 ms-5 '>
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPTION</th>
          <th>LOCALIZATION</th>
          <th>PHOTO</th>

        </tr>
      </thead>
{spaceData.map((space, index) => {
  return(
    <tr key={index}>
  <td>{space.id}</td>
  <td>{space.description}</td>
  <td>{space.localization}</td>
  <td><img src={space.photo} alt='Pics'/></td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>

    </div>

  );
}

export default ParkingSpace;
