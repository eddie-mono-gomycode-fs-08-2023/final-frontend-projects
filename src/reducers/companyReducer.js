
import { createSlice } from "@reduxjs/toolkit";
import { companyData } from "../FkeCompanyData";




export const companySlice = createSlice({
  name: 'companies',
  initialState: {value: companyData},
  reducers: {
    addCompany: (state, action) => {
state.value.push(action.payload)
    },
    
    deleteCompany: (state, action) => {
      state.value = state.value.filter((company) => company.id !== action.payload.id)
    },

    updateCompany: (state, action) => {
      state.value.map((company) =>  {    
          if (company.id === action.payload.id) {
            company.name = action.payload.name;
          }
          
          return company;
          
        })

    }
  }
})

export const {addCompany, updateCompany, deleteCompany} = companySlice.actions
export default companySlice.reducer
