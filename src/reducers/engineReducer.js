
import { createSlice } from "@reduxjs/toolkit";
import { engineData } from "../FkEngineData";



const engineSlice = createSlice({
  name: 'engines',
  initialState: {value: engineData},
  reducers: {
    addEngine: (state, action) => {
state.value.push(action.payload)
    },

    deleteEngine: (state, action) => {
      state.value = state.value.filter((engine) => engine.id !== action.payload.id)
    },

    updateEngine: (state, action) => {
      state.value.map((engine) =>  {    
          if (engine.id === action.payload.id) {
            engine.description = action.payload.description;
          }
          
          return engine;
          
        })
        }

    }
})


export const {addEngine, updateEngine, deleteEngine} = engineSlice.actions
export default engineSlice.reducer
