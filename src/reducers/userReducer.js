import { createSlice } from "@reduxjs/toolkit";
import { userData } from "../FakeData";
import axios from "axios";

export const userSlice = createSlice({
  name: 'users',
  initialState: {value: userData},
  reducers: {
    addUsers: (state, action) => {
state.value.push(action.payload)
axios.post('http://localhost:3300/user', action.payload)
.then(res => {
  console.log(res.data, 'Data has been added!')
})
.catch(err => console.log(err))
    },
    
    deleteUser: (state, action) => {
      state.value = state.value.filter((user) => user.id !== action.payload.id)
      axios.delete('http://localhost:3300/user', action.payload)
.then((res) => {
  console.log(res.data, 'Data has been deleted!')
})
.catch(err => console.log(err))
    },

    updateName: (state, action) => {
      state.value.map((user) =>  {    
          if (user.id === action.payload.id) {
            user.name = action.payload.name;
            user.surname = action.payload.surname;
            axios.put('http://localhost:3300/user', action.payload)
.then((res) => {
  console.log(res.data, 'Data has been updated!')
})
.catch(err => console.log(err))
          }
          
          return user;
          
        })

    }
  }
})
export const {addUsers, deleteUser, updateName} = userSlice.actions;
export default  userSlice.reducer;
