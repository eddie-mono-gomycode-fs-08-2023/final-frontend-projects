import { createSlice } from "@reduxjs/toolkit";
import { spaceData } from "../fkeSpaceData";
import axios from "axios";




const spaceSlice = createSlice({
  name: 'spaces',
  initialState: {value: spaceData},
  reducers: {
    addSpaces: (state, action) => {
    state.value.push(action.payload)
    },

    deleteSpace: (state, action) => {
      state.value = state.value.filter((space) => space.id !== action.payload.id)
    },

    updateSpace: (state, action) => {
      state.value.map((space) =>  {    
          if (space.id === action.payload.id) {
            space.description = action.payload.description;
            space.localization = action.payload.localization;
            axios.put('http://localhost:3300/space', action.payload)

          }
          
          return space;
          
        })

    }

    


  }
})

export const {addSpaces, updateSpace, deleteSpace} = spaceSlice.actions
export default spaceSlice.reducer
