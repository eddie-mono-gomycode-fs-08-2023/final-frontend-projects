import React from 'react';
import './Footer1.css'

const Footer1 = () => {
  return (
    <div className='futa-grid'>
    <div className='futa-flex'>

    <div className='Futa-items'>
      <h1>MonoPark</h1>
      <p>
      We are the official providers of Airport parking. <br/>
      You can't park closer!
      </p>
      </div>

      <div className='Futa-items'>
      <h1>Head Office</h1>
      <p>#215A Rue Principale De Yopougon</p>
      <p>Telephone: +225 0546897841</p>
      <p>Email: monopark@monopark.com</p>
      </div>

      <div className='Futa-items'>
      <h1>Our Services</h1>
      <ul>
      <li><a href='/wash'>Car Wash</a></li>
      <br/>
      <li><a href='/rentals'>Car Rentals</a></li>
      <br/>
      <li><a href='/mentain'>Car Maintenance</a></li>
      <br/>
      <li><a href='/parking'>Parking</a></li>
      </ul>
      </div>
    </div>

    </div>
      
  );
}

export default Footer1;
