import React from 'react';
import './RecordDrop.css'
import { Link } from 'react-router-dom';

const ServiceDrop = () => {
  return (
    <div class="dropdown">
  <span>Services</span>
  <div class="dropdown-content">

  <a href='wash'>Car Wash</a>
  <br/>
  {/* <Link to='users'>Use</Link> */}
  <br/>
  <a href='parking'>Parking</a>
  <br/>
  <br/>
  <a href='parkingspace'>Parking Space</a>
  <br/>
  <br/>
  <a href='rentals'>Car Rentals</a>
  <br/>
  <br/>
  <a href='mentain'>Mentainance</a>

  </div>
</div>
  );
}

export default ServiceDrop;
