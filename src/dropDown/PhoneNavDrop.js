import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown';

// import './PhoneDrop.css'


const PhoneNavDrop = () => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className='List-Drop1'>
        <button onClick={() => setIsOpen(!isOpen)}><h2>MENUS</h2></button>
        {
    isOpen && (
      <div className='List-dropitems'>
      <div>
        <Link to='home'>Home</Link> 
        </div>
        <div>
        <a href='parking'>Parking</a> 
        </div>
        <div>
        <a href='/'>Sign Out</a> 
        </div>
        </div>
    )
  }

  {/* <Dropdown>
      <Dropdown.Toggle variant="success" id="dropdown-basic">
        Dropdown Button
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown> */}


    </div>
  );
}

export default PhoneNavDrop;