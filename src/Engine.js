import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addEngine, updateEngine, deleteEngine } from './reducers/engineReducer';
import { Link } from 'react-router-dom';
import RecordDrop from './dropDown/RecordDrop';
import './Engine.css'


const Engine = () => {
  const dispatch = useDispatch();
  const EngineList = useSelector((state) => state.engines.value)
  console.log(EngineList);

  const [description, setDescription] = useState('')
  const [details, setDetails] = useState('')
  const [photo, setPhoto] = useState('')
  
  const [newDescription, setNewDescription] = useState('')

  return (
    <div>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    <RecordDrop/>
    <Link to='/'>Sign out</Link>
    </div>

    <div className='hello'><h1>Insert And View Records</h1></div>
    

    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Add Engine Information</h1>
      <div>
        <label htmlFor='description'>Description:</label>
        <input type='text' name='description' className='form-control' placeholder='Describe Your Engine' 
        onChange={e => setDescription(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='details'>Details:</label>
        <input type='text' name='details' className='form-control' placeholder='Enter Details' 
        onChange={e => setDetails(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
      </div><br/>
      <button onClick={() => {dispatch(addEngine({id: EngineList[EngineList.length - 1].id + 1, description, details, photo}))}}>Add User</button>
    </div>
    </div>

    <div className='container'>

    <h3>Engine Record</h3>

    {/* <Link to='/engineCreate' className='btn btn-success my-3'> Add Record +</Link> */}
    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>DESCRIPTION</th>
          <th>DETAILS</th>
          <th>PHOTO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{EngineList.map((engine, index) => {
  return(
    <tr key={index}>
  <td>{engine.id}</td>
  
  <td>{engine.description}</td>

  <td>{engine.details}</td>
  
  <td><img src={engine.photo} alt='Pic'/></td>
  <td>
  <input type='text' placeholder='New Description....' onChange={(e) => {setNewDescription(e.target.value)}}/>
    <button className='btn1' onClick={() => {dispatch(updateEngine({id: engine.id, description: newDescription}))}}>Update</button>
    <button className='btn2'  onClick={() => {dispatch(deleteEngine({id: engine.id}))}}>Delete Record</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>
    </div>

      
    </div>


  );
}

export default Engine;
