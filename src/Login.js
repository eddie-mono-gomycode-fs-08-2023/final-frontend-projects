import React, { useState } from 'react';
import './Layout.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';


const Login = () => {
  const Navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [authenticated, setAuthenticated] = useState(
    localStorage.getItem(localStorage.getItem("authenticated") || false)
  );
  const Users = [{ email: "eddymono148@gmail.com", password: "e123456789" }];
  const handleSubmit = (e) => {
    e.preventDefault();
    const account = Users.find((user) => user.email === email);
    if (account && account.password === password) {
      localStorage.setItem("authenticated", true);
      Navigate("/home");
    }else{
      alert('Wrong Credentials')
      Navigate('/login')
    }

}
  
  return (
    <div className='Layout-body'>
    <div className='Layouts'>
    <div className='Layouts-flex'>
      
      {/* Left Side */}

    <div className='Layout-left'>
    <img src='https://ppkm.org.my/wp-content/uploads/2017/05/Evolution_Car_Parks.jpg' alt='Park'/>  
    </div>

    
    {/* Right side */}

    <div className='Layout-right'>
    <h2>Sign in</h2>
    
    <div className='Layout-form'>
    <Form onSubmit={handleSubmit}>
      <Form.Group className="mb-2" controlId="formBasicEmail">
        {/* <Form.Label>Email address</Form.Label> */}
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
        <Form.Text className="text-muted">
          {/* We'll never share your email with anyone else. */}
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        {/* <Form.Label>Password</Form.Label> */}
        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Remember me" />
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>
      <Link to='/adminLog'>Login As Admin</Link>
      <br/>
      <div className='Layout-regBtn'>
      <h6>Don't have an account?<Link to='register'>Register</Link></h6>
      </div>
    </Form>
    </div>

    <div className='Copyright'>
      Copyright &copy; MonoPark
      </div>

</div>

</div>

</div>

<div className='Layout-down'>
<h2>hello</h2>
</div>

</div>
  );
}

export default Login;
