import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom';
import { addRequet } from './reducers/RequetCallReducer';

const RequetCreate = () => {
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')

const requets = useSelector((state) => state.requets.requets)
console.log(requets)
const dispatch = useDispatch()
const Navigate = useNavigate()

const createRequet = (e) => {
e.preventDefault()
dispatch(addRequet({id: Date.now(), name, phone}))
alert('Sent Successfully!')
Navigate('/requets')
}


return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border-none bg-tansparent text-white p-5'>
    <h1>Request A Call</h1>
    <form onSubmit={createRequet}>
      <div>
        <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='phone'>Telephone:</label>
        <input type='phone' name='phone' className='form-control' placeholder='Enter Phone Number' 
        onChange={e => setPhone(e.target.value)}/>
      </div>
      <br/>
      <button className='btn btn-info'>Request A Call</button>
    </form>
    </div>
    </div>
  );
}

export default RequetCreate;
