
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {addCompany, updateCompany, deleteCompany } from './reducers/companyReducer';
import { Link } from 'react-router-dom';
import RecordDrop from './dropDown/RecordDrop';

const Company = () => {
  const dispatch = useDispatch();
  const companyList = useSelector((state) => state.companies.value)
  console.log(companyList);

  const [name, setName] = useState('')
const [address, setAddress] = useState('')
const [email, setEmail] = useState('')
const [telephone, setTelephone] = useState('')
const [photo, setPhoto] = useState('')

const [newName, setNewName] = useState('')




  return (
    
  <div>
    <div className='Home-nav'>
    <Link to='/home'>Home</Link>
    <RecordDrop/>
    <Link to='/'>Sign out</Link>
    </div>


<div className='d-flex w-100 vh-100 justify-content-center align-items-center'>
    <div className='w-50 border bg-secondary text-white p-5'>
    <h1>Branches And Records</h1>
    <div>
      <label htmlFor='name'>Name:</label>
        <input type='text' name='name' className='form-control' placeholder='Enter Name' 
        onChange={e => setName(e.target.value)}/>
      </div>
      <div>
        <label htmlFor='address'>Address:</label>
        <input type='text' name='address' className='form-control' placeholder='Branch Address' 
        onChange={e => setAddress(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='telephone'>Telephone:</label>
        <input type='phone' name='telephone' className='form-control' placeholder='Enter Surname' 
        onChange={e => setTelephone(e.target.value)}/>
      </div>

      <div>
        <label htmlFor='photo'>Photo:</label>
        <input type='file' name='photo' className='form-control' placeholder='Upload Photo' 
        onChange={e => setPhoto(e.target.value)}/>
        </div><br/>
        <button onClick={() => {dispatch(addCompany({id: companyList[companyList.length - 1].id + 1, name, address, email, telephone, photo}))}}>Add User</button>
        </div>
        </div>


    <table className='table'>
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>ADDRESS</th>
          <th>EMAIL</th>
          <th>TELEPHONE</th>
          <th>PHOTO</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
{companyList.map((company, index) => {
  return(
    <tr key={index}>
  <td>{company.id}</td>
  
  <td>{company.name}</td>

  <td>{company.address}</td>

  <td>{company.email}</td>

  <td>{company.telephone}</td>

  <td><img src={company.photo} alt='company'/></td>
  <td>
  <input type='text' placeholder='New Name....' onChange={(e) => {setNewName(e.target.value)}}/>
    <button className='btn1' onClick={() => {dispatch(updateCompany({id: company.id, name: newName}))}}>Update</button>
    <button className='btn2'  onClick={() => {dispatch(deleteCompany({id: company.id}))}}>Delete Record</button>
</td>
</tr>
  )

})}
      <tbody>

      </tbody>

    </table>

      
    </div>
        

  );
}

export default Company;
